const {bigNumber} =  require ('./AddTwoNum');
const logger = require('./winston');

function Test(num1,num2){

    const resultTest = parseInt(num1) + parseInt(num2)
    if( bigNumber.sum(num1 , num2) === resultTest.toString())
    { 
      
        logger.info('Pass!');
    }
    else{
        logger.info('Fail!');
    }

  }
  logger.info('-------------TEST1---------------')
Test('1','1');
logger.info('-------------TEST2---------------')
Test('10','90');
logger.info('-------------TEST3---------------')
Test('999999','999999');
