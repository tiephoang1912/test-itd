const logger = require('./winston');

class MyBigNumber{

  sum(num1,num2) {
    const n1 = num1.length
    const n2 = num2.length
    const MaxNumLenght = (n1>n2)? n1:n2 ;
  
    let carry = 0;
    let result = ''

    for (let i = 0; i < MaxNumLenght; i++) {
        let N1 = ( n1-i-1>=0)?parseInt(num1[n1 - i -1]): 0 // Duyệt từng kí tự từ phải sang trái và chuyển thành số
        let N2 = ( n2-i-1>=0)?parseInt(num2[n2 - i -1]): 0
       
        // Cộng từng bước
        let temp = carry + N1 + N2
        carry = parseInt(temp/10) 
        let charTmp =parseInt(temp%10)
        
        logger.info(`${N1} + ${N2} = ${temp} carry: ${carry}`);
        result = charTmp + result

    }

    if(carry!==0){
      result = `1`+result
    }
    logger.info(`Result: ${result}`);
    return result

  }

}
module.exports = {
  bigNumber : new MyBigNumber()
}  ;

// console.log("Result is " + );
