## HOÀNG VĂN TIỆP - BÀI TEST ITDG
### HƯỚNG DẪN CHẠY CHƯƠNG TRÌNH
1. Vào thư mục source
2. Gõ `npm i` để cài node_modules winston 
3. Gõ `node UnitTest.js` để thực hiện test requiment
4. Có thể thay đổi tham số trong `Test(String:num1,String:num2)` để thực hiện test các giá trị khác.


Em rất cảm ơn ITDG đã cho em cơ hội thực hiện bài test tại công ty, bài làm của em có thể có nhiều sai sót,
rất mong anh/chị có thể xem xét. Một lần nữa em xin cảm ơn và chúc quý công ty luôn thành công ạ!